<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
ini_set('max_execution_time', -1);
ini_set('memory_limit', -1);

require_once('app/Mage.php');

Mage::app();

$collection = Mage::getModel('catalog/product')->getCollection();

$count_images = 0;
$urls = "";

foreach($collection as $product)
{
	$_product = Mage::getModel('catalog/product')->load($product->getId());
	$_gallery = $_product->getMediaGalleryImages();

	$count_images += count($_gallery);

	foreach($_gallery as $image)
	{
		$image_path = realpath('media/catalog/product/'.$image->getFile());
		echo $image_path.' ->'.Mage::helper('catalog/image')->init($_product->getProduct(), 'thumbnail', $image_path)->keepAspectRatio(TRUE)->resize(135,null)."\n";
	}
}

echo "$count_images thumbnail generated";
?>