<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

// Change current directory to the directory of current script
chdir(dirname(__FILE__));

require_once 'app/Mage.php';
Mage::app('default');
$storeViews = Mage::getModel('core/store')->getCollection();
foreach ($storeViews as $store) {
    if ($store->getId() == 0) {
        continue; //skip admin store
    }
    $collection = Mage::getModel('catalog/product')->setStoreId($store->getId())->getCollection()->addAttributeToSelect('*');
    foreach ($collection as $product) {
        //$metaDescription=Mage::helper('core/string')->substr($_product->getDescription(), 0, 255);
		$metaDescription=$product->getDescription();
		Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()),array('meta_description' => $metaDescription),$store->getId());
    }
}