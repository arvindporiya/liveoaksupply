<?php
ini_set('max_execution_time', 72000);
ini_set('memory_limit','2024M');
require_once 'app/Mage.php';
Mage::app('default');
$_productCollection = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addAttributeToSort('created_at', 'ASC')
                        ->addAttributeToSelect('*')
						->addAttributeToFilter('visibility', 4)
						//->addAttributeToFilter('sku', ['in' => array('NRS18200','NRS18200A','NRS18200B')])
						//->setPageSize(10)
						//->setCurPage(1)
                        ->load();
	$myFile = "rss.xml";
	$fh = fopen($myFile, 'w') or die("can't open file");
	$rss_txt .= '<?xml version="1.0" encoding="utf-8"?>';
	$rss_txt .= '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
	$rss_txt .= '<channel>';
	$rss_txt .= '<title>Products From Live Oak Supply</title>';
	$rss_txt .= '<link>https://www.liveoaksupply.com</link>';
	$rss_txt .= '<description>A description of your content</description>';	
	//$rss_txt .= '<g:link>'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'index.php/'.$_product->getData("url_path").'</g:link>';					
foreach ($_productCollection as $_product){
			
   			if($_product->getData("sku") =='')
				$sku = $_product->getData("entity_id");
			else
				$sku = $_product->getData("sku");
			if($_product->getAttributeText('brand_name') =='')
				$brand_name = 'Live Oak Supply';
			else
				$brand_name = $_product->getAttributeText('brand_name');
			if($_product->getPrice() ==0)
				$price = '1.00';
			else
				$price = number_format($_product->getPrice(), 2, '.', '');		
			$rss_txt .= '<item>';
			$rss_txt .= '<g:id>' .$sku. '</g:id>';
			$rss_txt .= '<g:title>' .$_product->getData("name"). '</g:title>';
			$rss_txt .= '<g:description>'.trim(html_entity_decode(strip_tags($_product->getData("short_description")))).'</g:description>';
			$rss_txt .= '<g:link>'.Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).'index.php/catalog/product/view/id/'.$_product->getData("entity_id").'</g:link>';
			$rss_txt .= '<g:image_link>'.$_product->getImageUrl().'</g:image_link>';
			$rss_txt .= '<g:condition>new</g:condition>';
			$rss_txt .= '<g:availability>in stock</g:availability>';
			$rss_txt .= '<g:price>'.$price.'</g:price>';
			$rss_txt .= '<g:shipping>';
			$rss_txt .= '<g:country>US</g:country>';
			$rss_txt .= '<g:service>Standard</g:service>';
			$rss_txt .= '<g:price>1 USD</g:price>';
			$rss_txt .= '</g:shipping>';
			$rss_txt .= '<g:tax>';
			$rss_txt .= '<g:country>US</g:country>';
			$rss_txt .= '<g:region>CA</g:region>';
			$rss_txt .= '<g:rate>1.00</g:rate>';
			$rss_txt .= '<g:tax_ship>yes</g:tax_ship>';
			$rss_txt .= '</g:tax>';
			//$rss_txt .= '<g:gtin>' .$_product->getData("entity_id"). '</g:gtin>';
			$rss_txt .= '<g:brand>'.$brand_name.'</g:brand>';
			$rss_txt .= '<g:mpn>' .$sku. '</g:mpn>';
			$rss_txt .= '</item>';
	}
	$rss_txt .= '</channel>';
	$rss_txt .= '</rss>';
	fwrite($fh, $rss_txt);
	fclose($fh);
	header('Content-disposition: attachment; filename="'.$myFile.'"');
	readfile(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$myFile);
	exit;
	
?>