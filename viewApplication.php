<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE>Credit Application</TITLE>
<STYLE type="text/css">

body {margin-top: 0px;margin-left: 0px;}

#page_1 {position:relative; overflow: hidden;margin: 24px auto 52px auto;padding: 0px;border: none;width: 776px;}

#page_1 #dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:576px;height:713px;}
#page_1 #dimg1 #img1 {width:576px;height:713px;}




.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: bold 19px 'Arial';line-height: 22px;}
.ft1{font: 15px 'Arial';line-height: 19px;}
.ft2{font: bold 16px 'Arial';line-height: 19px;}
.ft3{font: bold 14px 'Arial';line-height: 16px;}
.ft4{font: 11px 'Arial';line-height: 14px;}
.ft5{/*font: 1px 'Arial';line-height: 1px;*/}
.ft6{font: 10px 'Arial';line-height: 13px;}
.ft7{font: 12px 'Arial';line-height: 15px;}
.ft8{font: 1px 'Arial';line-height: 10px;}
.ft9{font: 1px 'Arial';line-height: 11px;}
.ft10{font: 1px 'Arial';line-height: 14px;}
.ft11{font: bold 12px 'Arial';line-height: 15px;}
.ft12{font: bold 12px 'Arial';line-height: 12px;}
.ft13{font: 1px 'Arial';line-height: 12px;}
.ft14{font: bold 11px 'Arial';line-height: 14px;}
.ft15{font: bold 12px 'Arial';line-height: 14px;}
.ft16{font: 8px 'Arial';line-height: 12px;}
.ft17{font: bold 8px 'Arial';line-height: 12px;}

.p0{text-align: left;padding-left: 124px;margin-top: 62px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 134px;padding-right: 169px;margin-top: 0px;margin-bottom: 0px;text-indent: 43px;}
.p2{text-align: left;padding-left: 72px;margin-top: 0px;margin-bottom: 0px;}
.p3{text-align: left;padding-left: 266px;margin-top: 9px;margin-bottom: 0px;}
.p4{text-align: left;padding-left: 8px;margin-top: 24px;margin-bottom: 0px;}
.p5{text-align: left;padding-left: 8px;margin-top: 13px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 32px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: left;padding-left: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-left: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 78px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;padding-left: 53px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 8px;margin-top: 6px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 8px;margin-top: 11px;margin-bottom: 0px;}
.p20{text-align: left;padding-left: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: left;padding-left: 51px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: left;padding-left: 34px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: left;padding-left: 39px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: left;padding-left: 73px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p25{text-align: left;padding-left: 64px;margin-top: 0px;margin-bottom: 0px;}
.p26{text-align: left;padding-left: 8px;padding-right: 45px;margin-top: 11px;margin-bottom: 0px;}
.p27{text-align: left;padding-left: 8px;margin-top: 22px;margin-bottom: 0px;}
.p28{text-align: justify;padding-left: 8px;padding-right: 63px;margin-top: 11px;margin-bottom: 0px;}
.p29{text-align: left;padding-left: 8px;margin-top: 23px;margin-bottom: 0px;}

.td0{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 160px;vertical-align: bottom;}
.td1{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 14px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 9px;vertical-align: bottom;}
.td3{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 60px;vertical-align: bottom;}
.td4{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
.td5{padding: 0px;margin: 0px;width: 13px;vertical-align: bottom;}
.td6{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 205px;vertical-align: bottom;}
.td7{border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 11px;vertical-align: bottom;}
.td8{border-left: #000000 1px solid;border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 31px;vertical-align: bottom;}
.td9{padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
.td10{padding: 0px;margin: 0px;width: 17px;vertical-align: bottom;}
.td11{padding: 0px;margin: 0px;width: 80px;vertical-align: bottom;}
.td12{padding: 0px;margin: 0px;width: 586px;vertical-align: bottom;}
.td13{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 16px;vertical-align: bottom;}
.td14{padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
.td15{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 17px;vertical-align: bottom;}
.td16{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
.td17{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
.td18{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 369px;vertical-align: bottom;}
.td19{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 184px;vertical-align: bottom;}
.td20{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 22px;vertical-align: bottom;}
.td21{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 11px;vertical-align: bottom;}
.td22{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
.td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
.td24{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 80px;vertical-align: bottom;}
.td25{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 183px;vertical-align: bottom;}
.td26{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 61px;vertical-align: bottom;}
.td27{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 122px;vertical-align: bottom;}
.td28{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 183px;vertical-align: bottom;}
.td29{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 133px;vertical-align: bottom;}
.td30{border-left: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 160px;vertical-align: bottom;}
.td31{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 8px;vertical-align: bottom;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 12px;vertical-align: bottom;}
.td33{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
.td34{border-left: #000000 1px solid;border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: bottom;}
.td35{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 145px;vertical-align: bottom;}
.td36{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 109px;vertical-align: bottom;}
.td37{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td38{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 222px;vertical-align: bottom;}
.td39{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: bottom;}
.td40{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 145px;vertical-align: bottom;}
.td41{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 109px;vertical-align: bottom;}
.td42{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td43{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 222px;vertical-align: bottom;}

.tr0{height: 19px;}
.tr1{height: 17px;}
.tr2{height: 26px;}
.tr3{height: 10px;}
.tr4{height: 11px;}
.tr5{height: 14px;}
.tr6{height: 15px;}
.tr7{height: 28px;}
.tr8{height: 12px;}
.tr9{height: 27px;}

.t0{width: 736px;margin-top: 10px;font: bold 12px 'Arial';}
.t1{width: 735px;font: bold 12px 'Arial';}

</STYLE>
</HEAD>

<BODY>
<?php 
$hostname = 'localhost';
$username = 'liveoaksupply_m4';
$password = '@S9Y64Pf3]';
$databasename='liveoaksupply_m4';
$con = mysql_connect($hostname,$username,$password);
$db_selected = mysql_select_db($databasename, $con);
$str = '';
$str1 = '';
error_reporting(0);
$query = 'select * from mg_credit_application where id = '.$_GET['id'].' limit 1';

$result = mysql_query($query) or die(mysql_error());
if (mysql_num_rows($result)>0){
	$row = mysql_fetch_assoc($result);
}
$temp = explode(',',$row['type_of_business']);
?>
<DIV id="page_1">
<DIV id="dimg1">
<IMG src="CreditApplication1x1.jpg" id="img1">
</DIV>


<DIV class="dclr"></DIV>
<P class="p0 ft0">“Where Quality Products Meet Excellent Service” Since 1986</P>
<P class="p1 ft1">4225 Steve Reynolds Boulevard • Norcross, Georgia 30093 Phone: (770) <NOBR>963-3000</NOBR> • Fax: (770) <NOBR>963-3136</NOBR> • www.liveoaksupply.com</P>
<P class="p2 ft2">__________________________________________________________________________</P>
<P class="p3 ft3"><SPAN class="ft0">A</SPAN>PPLICATION FOR <SPAN class="ft0">C</SPAN>REDIT</P>
<P class="p4 ft4">Company Name: <span class="val compnay_name"><?php echo $row['compnay_name'];?></span></P>
<P class="p5 ft4">Physical Address: <?php echo $row['physical_address'];?> Mailing Address: <?php echo $row['mailing_address'];?></P>
<P class="p5 ft4">Telephone #: <?php echo $row['telephone'];?> Fax #: <?php echo $row['fax'];?></P>
<P class="p5 ft4">City/State/Zip: <?php echo $row['city_state_zip'];?> Website: <?php echo $row['website'];?></P>
<P class="p5 ft4">Credit Limit Requested: <?php echo $row['credit_limit'];?> Date Business Opened: <?php echo $row['business_opened'];?> Social Security or EIN Number: <?php echo $row['ein'];?></P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p6 ft4">Type of Business: Corporation</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="checkbox" class="validate[minCheckbox[1]]" name="type_of_business[]" id="type_of_business1"  value="Corporation" <?php if(in_array('Corporation',$temp)){ ?> checked="checked" <?php }else{}?>/></P></TD>
	<TD class="tr0 td2"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr0 td3"><P class="p8 ft4">Partnership</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="checkbox" class="validate[minCheckbox[1]]" name="type_of_business[]" id="type_of_business2" value="Partnership" <?php if(in_array('Partnership',$temp)){ ?> checked="checked" <?php }else{}?>/></P></TD>
	<TD class="tr0 td4"><P class="p9 ft4">Proprietorship</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="checkbox" class="validate[minCheckbox[1]]" name="type_of_business[]" id="type_of_business3" value="Proprietorship" <?php if(in_array('Proprietorship',$temp)){ ?> checked="checked" <?php }else{}?>/></P></TD>
	<TD class="tr0 td5"><P class="p7 ft5">&nbsp;</P></TD>
	<TD colspan=2 class="tr0 td6"><P class="p10 ft6">Are purchase orders required? Yes</P></TD>
	<TD class="tr1 td7"><P class="p7 ft5">&nbsp;<input type="radio" class="validate[required]" name="purchase_orders" id="purchase_orders" value="Yes" <?php if($row['purchase_orders']=='Yes'){?> checked="checked" <?php }else{}?>/></P></TD>
	<TD colspan=2 class="tr0 td8"><P class="p11 ft7">No</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="radio" class="validate[required]" name="purchase_orders" id="purchase_orders" value="No" <?php if($row['purchase_orders']=='No'){?> checked="checked" <?php }else{}?>/></P></TD>
	<TD class="tr0 td9"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr0 td10"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr0 td11"><P class="p7 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=11 rowspan=2 class="tr2 td12"><P class="p6 ft4">Have you ever filed for bankruptcy (answer yes even if the bankruptcy was filed under another company name)? Yes</P></TD>
	<TD class="tr3 td13"><P class="p7 ft8">&nbsp;</P></TD>
	<TD colspan=3 rowspan=2 class="tr2 td14"><P class="p12 ft7">No</P></TD>
	<TD class="tr3 td15"><P class="p7 ft8">&nbsp;</P></TD>
	<TD class="tr4 td11"><P class="p7 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr5 td16"><P class="p7 ft10">&nbsp;<input type="radio" class="validate[required]" name="filed_for_bankruptcy" id="filed_for_bankruptcy" value="Yes" <?php if($row['filed_for_bankruptcy']=='Yes'){?> checked="checked" <?php }else{}?>/></P></TD>
	<TD class="tr5 td17"><P class="p7 ft10">&nbsp;<input type="radio" class="validate[required]" name="filed_for_bankruptcy" id="filed_for_bankruptcy" value="No" <?php if($row['filed_for_bankruptcy']=='No'){?> checked="checked" <?php }else{}?>/></P></TD>
	<TD class="tr6 td11"><P class="p7 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=8 class="tr7 td18"><P class="p6 ft11">Name of President and Treasurer, Owner(s) or Partners:</P></TD>
	<TD class="tr7 td19"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td20"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td21"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td13"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td15"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td22"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td23"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td15"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr7 td24"><P class="p7 ft5">&nbsp;</P></TD>
</TR>
</table>
<TABLE cellpadding=0 cellspacing=0 class="t0" border="1">
<TR>
	<TD ><P >Name</P></TD>
	<TD ><P >Address</P></TD>
	<TD ><P >Direct Phone #</P></TD>
	<TD ><P >Email Address</P></TD>
</TR>
<TR>
	<TD ><P >&nbsp;<?php echo $row['president_name1'];?></P></TD>
	<TD ><P >&nbsp;<?php echo $row['president_address1'];?></P></TD>
	<TD ><P >&nbsp;<?php echo $row['president_direct_phone1'];?></P></TD>
	<TD ><P >&nbsp;<?php echo $row['president_email1'];?></P></TD>
</TR>
<TR>
	<TD ><P >&nbsp;<?php echo $row['president_name2'];?></P></TD>
	<TD ><P >&nbsp;<?php echo $row['president_address2'];?></P></TD>
	<TD ><P >&nbsp;<?php echo $row['president_direct_phone2'];?></P></TD>
	<TD ><P >&nbsp;<?php echo $row['president_email2'];?></P></TD>
</TR>
</TABLE>
<P class="p17 ft4">A/P Contact: <?php echo $row['ap_contact'];?> Title: <?php echo $row['ap_title'];?></P>
<P class="p5 ft4">Telephone #: <?php echo $row['ap_telephone'];?> Email Address: <?php echo $row['ap_email'];?></P>
<P class="p5 ft11">List bank account(s):</P>
<P class="p5 ft14"><?php echo $row['bank_info'];?></P>
<P class="p18 ft7">Bank Reference & Account Holder Account # Address Telephone #</P>
<P class="p19 ft11">Please list all business/trade References (this MUST be businesses in which you purchase tools and/or materials, NOT subcontractors):</P>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr5 td34"><P class="p20 ft15">Trade Reference Name</P></TD>
	<TD class="tr5 td35"><P class="p21 ft15">Address</P></TD>
	<TD class="tr5 td36"><P class="p22 ft15">Phone #</P></TD>
	<TD class="tr5 td37"><P class="p23 ft15">Fax #</P></TD>
	<TD class="tr5 td38"><P class="p24 ft15">Email Address</P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<?php echo $row['trade_ref_no1'];?></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<?php echo $row['trade_address1'];?></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<?php echo $row['trade_phone1'];?></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<?php echo $row['trade_fax1'];?></P></TD>
	<TD class="tr9 td43"><P class="p7 ft5">&nbsp;<?php echo $row['trade_email1'];?></P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<?php echo $row['trade_ref_no2'];?></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<?php echo $row['trade_address2'];?></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<?php echo $row['trade_phone2'];?></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<?php echo $row['trade_fax2'];?></P></TD>
	<TD class="tr9 td43"><P class="p7 ft5">&nbsp;<?php echo $row['trade_email2'];?></P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<?php echo $row['trade_ref_no3'];?></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<?php echo $row['trade_address3'];?></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<?php echo $row['trade_phone3'];?></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<?php echo $row['trade_fax3'];?></P></TD>
	<TD class="tr9 td43"><P class="p7 ft5">&nbsp;<?php echo $row['trade_email3'];?></P></TD>
</TR>
<TR>
	<TD class="tr2 td39"><P class="p7 ft5">&nbsp;<?php echo $row['trade_ref_no4'];?></P></TD>
	<TD class="tr2 td40"><P class="p7 ft5">&nbsp;<?php echo $row['trade_address4'];?></P></TD>
	<TD class="tr2 td41"><P class="p7 ft5">&nbsp;<?php echo $row['trade_phone4'];?></P></TD>
	<TD class="tr2 td42"><P class="p7 ft5">&nbsp;<?php echo $row['trade_fax4'];?></P></TD>
	<TD class="tr2 td43"><P class="p7 ft5">&nbsp;<?php echo $row['trade_email4'];?></P></TD>
</TR>
</TABLE>
<P class="p25 ft11">**PLEASE NOTE: HD SUPPLY/WHITE CAP, ARGOS, LAFARGE & UNITED RENTAL WILL NOT QUALIFY**</P>
<P class="p26 ft16">Credit terms are 30 days from date of invoice. Outstanding balances are subject to 1.5% per month interest. The undersigned authorizes and releases all banks, persons, and companies listed on this application to furnish information and authorize the checking of credit. The undersigned agrees to pay all collection costs, court costs, and legal fees incurred to collect delinquent balances.</P>
<P class="p27 ft4">________________________________________________________________________________________________________________________</P>
<P class="p18 ft7">Print Name Title Signature Date</P>
<P class="p28 ft16"><SPAN class="ft17">PERSONAL GUARANTY</SPAN>: In consideration of credit being extended to the above named firm, I personally guarantee all indebtedness hereunder. I further agree that this guarantee is an absolute, complete and continuing one and no notice of indebtedness or any extension of credit already or hereafter contracted by or extended need be given. The terms may be rearranged, extended and/or renewed without notice to me. I will, within five days from date of notice that the account is past due, pay the amount due.</P>
<P class="p29 ft4">Name: <?php echo $row['name'];?> SSN: <?php echo $row['ssn'];?> Signature: ____________________________________________</P>
<P class="p5 ft4">Home Address: <?php echo $row['home_address'];?></P>
<P class="p5 ft4">Direct Phone #: <?php echo $row['direct_phone'];?> Driver’s License #: <?php echo $row['license_no'];?> Date: <?php echo $row['date'];?></P>
</DIV>
</BODY>
</HTML>
