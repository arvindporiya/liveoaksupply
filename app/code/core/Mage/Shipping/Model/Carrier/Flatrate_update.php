<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Shipping
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Flat rate shipping model
 *
 * @category   Mage
 * @package    Mage_Shipping
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Mage_Shipping_Model_Carrier_Flatrate
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{

    protected $_code = 'flatrate';
    protected $_isFixed = true;

    /**
     * Enter description here...
     *
     * @param Mage_Shipping_Model_Rate_Request $data
     * @return Mage_Shipping_Model_Rate_Result
     */
	 
	//Mujahid funtion to get Distance 
	public function curl_request($sURL,$sQueryString=null){
		$cURL=curl_init();
		curl_setopt($cURL,CURLOPT_URL,$sURL.'?'.$sQueryString);
		curl_setopt($cURL,CURLOPT_RETURNTRANSFER, TRUE);
		$cResponse=trim(curl_exec($cURL));
		curl_close($cURL);
		return $cResponse;
	}
	public function get_driving_information($start,$finish){
		if($finish !=""){
			if(strcmp($start, $finish) == 0){
				$time = 0;
				if($raw){
					$time .= ' seconds';
				}
				return array('distance' => 0, 'time' => $time);
			}
			$start  = urlencode($start);
			$finish = urlencode($finish);
			$distance   = 0;
			$time       = 0;
			$sResponse= $this->curl_request('http://maps.googleapis.com/maps/api/distancematrix/json','origins='.str_replace(" ","+",strtolower($start)).'&destinations='.str_replace(" ","+",strtolower($finish)).'&mode=driving&units=imperial&sensor=false');
			$oJSON=json_decode($sResponse);
			if ($oJSON->status=='OK'){
					$fDistanceInMiles=(int)preg_replace('/[^\d\.]/','',$oJSON->rows[0]->elements[0]->distance->text);
					$fDurationInSec = (int)preg_replace('/[^\d\.]/','',$oJSON->rows[0]->elements[0]->duration->value);
			}else{
					$fDistanceInMiles=0;
					$fDurationInSec=0;
			}
			return array('distance' => round($fDistanceInMiles), 'time' => $fDurationInSec);
		}
	} 
	///////////////////////////////////////////////////////////////////////////////////
    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
		if(($request->getBaseSubtotalInclTax() < 250)){
				return false;
		}
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {

                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);

        $result = Mage::getModel('shipping/rate_result');
        if ($this->getConfigData('type') == 'O') { // per order
            $shippingPrice = $this->getConfigData('price');
        } elseif ($this->getConfigData('type') == 'I') { // per item
            $shippingPrice = ($request->getPackageQty() * $this->getConfigData('price')) - ($this->getFreeBoxes() * $this->getConfigData('price'));
        } else {
            $shippingPrice = false;
        }

        $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);

        if ($shippingPrice !== false) {
            $method = Mage::getModel('shipping/rate_result_method');

            $method->setCarrier('flatrate');
            $method->setCarrierTitle($this->getConfigData('title'));

            $method->setMethod('flatrate');
            $method->setMethodTitle($this->getConfigData('name'));

            if ($request->getFreeShipping() === true || $request->getPackageQty() == $this->getFreeBoxes()) {
                $shippingPrice = '0.00';
            }
			//Mujahid Code For shipping
			$store_street = Mage::getStoreConfig('shipping/origin/street_line1');
			$store_city = Mage::getStoreConfig('shipping/origin/city');
			$store_postcode = Mage::getStoreConfig('shipping/origin/postcode');
			$store_state = 'GA';
			$store_country ='US';
			$store_address = $store_street." ".$store_city.", ".$store_state." ".$store_postcode." ".$store_country;
			$post = $_POST['billing'];
			if($post['use_for_shipping']==1){
				$region = Mage::getModel('directory/region')->load($post['region_id']);
				$state_code = $region->getCode();
				$address = implode(" ",$post['street'])." ".$post['city'].", ".$state_code." ".$post['postcode']." ".$post['country_id'];
			}else{
				$post = $_POST['shipping'];
				$region = Mage::getModel('directory/region')->load($post['region_id']);
				$state_code = $region->getCode();
				$address = implode(" ",$post['street'])." ".$post['city'].", ".$state_code." ".$post['postcode']." ".$post['country_id'];
			}
			
			$quote = Mage::getSingleton('checkout/session')->getQuote();
			$shippingAddress = $quote->getShippingAddress();
			$street = $shippingAddress->getStreet();
			$city = $shippingAddress->getCity();
			$region_id  = $shippingAddress->getRegionId();
			$postcode  = $shippingAddress->getPostcode();
			
			$region = Mage::getModel('directory/region')->load($region_id);
			$state_code = $region->getCode();
			$address = implode(" ",$street)." ".$city.", ".$state_code." ".$postcode." US";
			
			$distanceDriving = $this->get_driving_information($store_address,$address);
			$distance_miles = $distanceDriving['distance'];
			if($distance_miles > 0 && $distance_miles<= 39.99){
				$shippingPrice = 20;
			}elseif($distance_miles >= 40 && $distance_miles<= 59.99){
				$shippingPrice = 35;
			}else{
				$baseshippingPrice = 35;
				if($distance_miles>0){
					$remaining_distance = $distance_miles - 60;
					$extra_shipping = $remaining_distance * 1.5;
					$shippingPrice = $baseshippingPrice + $extra_shipping; 
				}
			}
			/////////////////////////////////////////////////////////
			//$shippingPrice = 35;
            $method->setPrice($shippingPrice);
            $method->setCost($shippingPrice);
			
            $result->append($method);
        }

        return $result;
    }

    public function getAllowedMethods()
    {
        return array('flatrate'=>$this->getConfigData('name'));
    }

}
