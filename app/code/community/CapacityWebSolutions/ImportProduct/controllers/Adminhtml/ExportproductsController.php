<?php
/***************************************************************************
	@extension	: Import Products categories, multiple images and custom options
	@copyright	: Copyright (c) 2014 Capacity Web Solutions.
	( http://www.capacitywebsolutions.com )
	@author		: Capacity Web Solutions Pvt. Ltd.
	@support	: magento@capacitywebsolutions.com
	
***************************************************************************/

class  CapacityWebSolutions_ImportProduct_Adminhtml_ExportproductsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() 
	{
	$this->loadLayout()
			->_setActiveMenu('cws')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
		
		
	} 
	
	function is_connected()
	{
		$connected = @fsockopen("www.google.com", 80); 
		//website, port  (try 80 or 443)
		if ($connected){
			$is_conn = true;
			fclose($connected);
		}else{
			$is_conn = false;
		}
		return false;

	}
	
	public function indexAction() {
	
	
		
		  $model = Mage::getModel('importproduct/profile');	
		  $headers = array(
				'Content-Type: text/xml; charset=utf-8'
		   );

		  // Build the cURL session
		  $ch = curl_init();
		  curl_setopt($ch, CURLOPT_URL, $model->getWebServiceURL());
		  curl_setopt($ch, CURLOPT_POST, FALSE);
		  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);


		$whitelist = array(
			'127.0.0.1',
			'::1'
		);

		if(!in_array($_SERVER['SERVER_ADDR'], $whitelist) &&  $this->is_connected()){
		  // Send the request and check the response

		  if (($result = curl_exec($ch)) === FALSE) {
				$data='Your serial key not matchs with our record. Please contacts us.';
				Mage::getSingleton("core/session")->addError($data); 

		  } else 
		  {
			$xml = simplexml_load_string($result);			
			if($xml->cws->status==1){
			//Mage::register('','license_msg');
			}else{
				$data='Your serial key not matchs with our record. Please contacts us.';
				Mage::getSingleton("core/session")->addError($data); 
			}
		}
		}else{
		}
		curl_close($ch);
				
				
		Mage::register('license_msg', $data);// In the Controller


		$this->_initAction();		
		if($data==null){		
		$this->_addContent($this->getLayout()->createBlock('importproduct/adminhtml_exportproducts_edit'))
             ->_addLeft($this->getLayout()->createBlock('importproduct/adminhtml_exportproducts_edit_tabs'));			 			 
		}
		$this->renderLayout();		
		
		Mage::getSingleton("core/session")->addNotice("Note : Exported file will be save in var/export directory.");
		
	}

	public function downloadExportedFileAction(){
	
		$filename=Mage::app()->getRequest()->getParam('file');
        $filepath = Mage::getBaseDir('base').'/var/export/'.$filename;

        if (! is_file ( $filepath ) || ! is_readable ( $filepath )) {
            throw new Exception ( );
        }
        $this->getResponse ()
                    ->setHttpResponseCode ( 200 )
                    ->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true )
                     ->setHeader ( 'Pragma', 'public', true )
                    ->setHeader ( 'Content-type', 'application/force-download' )
                    ->setHeader ( 'Content-Length', filesize($filepath) )
                    ->setHeader ('Content-Disposition', 'attachment' . '; filename=' . basename($filepath) );
        $this->getResponse ()->clearBody ();
        $this->getResponse ()->sendHeaders ();
        readfile ( $filepath );
        exit;
		
	
	}
	public function downloadExportedFileFromGridAction(){
	
		$file_id=Mage::app()->getRequest()->getParam('id');
		$file=Mage::getModel('importproduct/exportedfile')->load($file_id);
        $filename=$file->getFileName(); 
		
		$filepath = Mage::getBaseDir('base').'/var/export/'.$filename;

        if (! is_file ( $filepath ) || ! is_readable ( $filepath )) {
            throw new Exception ( );
        }
        $this->getResponse ()
                    ->setHttpResponseCode ( 200 )
                    ->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true )
                     ->setHeader ( 'Pragma', 'public', true )
                    ->setHeader ( 'Content-type', 'application/force-download' )
                    ->setHeader ( 'Content-Length', filesize($filepath) )
                    ->setHeader ('Content-Disposition', 'attachment' . '; filename=' . basename($filepath) );
        $this->getResponse ()->clearBody ();
        $this->getResponse ()->sendHeaders ();
        readfile ( $filepath );
        exit;
		
	
	}

	
	public function deleteExportedFileFromGridAction(){
	
		$file_id=Mage::app()->getRequest()->getParam('id');
		$file=Mage::getModel('importproduct/exportedfile')->load($file_id);   
		$filename=$file->getFileName();					
		$file->delete();
		$baseDir = Mage::getBaseDir();		
		$filepath = $baseDir.DS.'var'.DS.'export'.DS.$filename;						
		
		if(file_exists($filepath)){
			unlink($filepath);
			Mage::getSingleton("core/session")->addSuccess("File: ".$filename." successfully deleted."); 
		}else{
				Mage::getSingleton("core/session")->addError("File: ".$filename." does not exists.");			
		}
		
		$this->_redirect('*/*/index');

	}	
	
}
?>