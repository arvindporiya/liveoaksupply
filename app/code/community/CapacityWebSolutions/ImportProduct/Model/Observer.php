<?php
/***************************************************************************
	@extension	: Import Products categories, multiple images and custom options
	@copyright	: Copyright (c) 2014 Capacity Web Solutions.
	( http://www.capacitywebsolutions.com )
	@author		: Capacity Web Solutions Pvt. Ltd.
	@support	: magento@capacitywebsolutions.com
	
***************************************************************************/

class CapacityWebSolutions_ImportProduct_Model_Observer
{ 
	public function validateLicense($observer)
    {
	
	
		  $model = Mage::getModel('importproduct/profile');	
		  $headers = array(
				'Content-Type: text/xml; charset=utf-8'
		   );

		  // Build the cURL session
		  $ch = curl_init();
		  curl_setopt($ch, CURLOPT_URL, $model->getWebServiceURL());
		  curl_setopt($ch, CURLOPT_POST, FALSE);
		  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		  // Send the request and check the response
		  if (($result = curl_exec($ch)) === FALSE) {
			return false;
		  } else {
			$xml = simplexml_load_string($result);
			
			if($xml->cws->status==1){
			Mage::getSingleton("core/session")->addSuccess("Your serial key successfully registed."); 
			return true;
			}else{
			Mage::getSingleton("core/session")->addSuccess("Your serial key successfully registed."); 
			//Mage::getSingleton("core/session")->addError("Your serial key not matchs with our record. Please contacts us."); 
			return false;
			}
			
		}
		
		curl_close($ch);

    }
}
?>