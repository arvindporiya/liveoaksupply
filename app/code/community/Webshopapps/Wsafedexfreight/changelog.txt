1.0	- Initial version
1.1	- Modifyed to lookup store address in common to resolve backward compatibilty issues with mainly Magento 1.4.1
1.2 	- Supports always displaying some carriers when freight rates found e.g. pickup
1.3 	- Changed to work with wsafreightcommon
1.4	- Changed to use Wsafreightcommon.
2.0	- Updated to work with latest freightcommon
2.1	- Include latest common code
2.2	- Include latest common code
2.3	- Minor performance enhancements
2.4  	- Minor Enhancements around logging.
2.5 	- Improved the functionality of accessories. 
3.0 	- Repackaged with the latest Common and Freight Common
4.0	- All files moved to commutiy folder
4.1 	- FREIGHT-34 Added support for FedEx account discount
4.3 	- FREIGHT-42 Removed the city field from FedEx Freight
5.0 	- FREIGHT-59 Official release of refactored extension without extra step in checkout
5.1     - FREIGHT-94 Changed LIST ACCOUNT logic to look at separate parts of rate response
5.2	- New Freight Common
5.3  - FREIGHT-111 Added the extra fields in the Fedex Freight Tab in the Warehouse settings
5.2.2  - new version of freight common
5.2.3  - New version of Freight Common
5.2.4  - SHIPMAN-7 Updated to include latest FreightCommon
5.2.5  - Ensuring latest version of FreightCommon
5.2.6  - New FreightCommon
5.2.7  - FREIGHT-135 Compatibility with IWD OPC
5.2.8  - Freight Common 4.5
5.2.9  - FREIGHT-140 Freight information no longer displayed if no freight items are in the cart.
5.2.9  - Includes latest Freight Commom
5.2.10  - New FreightCommon
5.2.11  - New FreightCommon
5.2.12  - FREIGHT-143 - Fixed logic around free shipping promotions within the checkout
5.2.13  - New FreightCommon
5.2.14  - New FreightCommon
5.2.15  - Updated Common
5.2.16  - Latest version of Freight common
5.2.17  - New FreightCommon
5.2.18  - New FreightCommon
5.2.19  - FREIGHT-157 Resolved issue when not using live accessories in FedEx.
5.2.20  - New FreightCommon
5.2.21  - New Freight Common
5.2.22  - New FreightCommon
5.2.23  - New FreightCommon
5.2.24  - New FreightCommon
5.2.25  - DIMSHIP-147 - Added minimum package length option to work with Dimensional Shipping when installed
5.2.26  - New FreightCommon
5.2.27  - New FreightCommon
5.2.28  - New FreightCommon
5.2.29  - FREIGHT-172 - Checks for validity of notify and inside delivery per carrier
5.2.30  - FREIGHT-177 - resolved accessorials via ajax not setting correctly
5.2.31  - FREIGHT-178 - Altered param name for res selector at checkout
5.2.32  - New FreightCommon
5.2.33  - New FreightCommon
5.2.34  - New FreightCommon
5.2.35  - FREIGHT-182 Fixed issue with Always add Business Fee not working correctly
5.2.36  - COMMON-39 Reverted getProduct() change as wasnt working on earlier magento versions
5.2.37  - New Freight Common
5.2.38  - New FreightCommon
5.2.39  - New FreightCommon
5.2.40  - New FreightCommon
5.2.41  - New FreightCommon
5.2.42  - New FreightCommon
5.2.43  - New WSA Common
5.2.44  - New FreightCommon
5.2.45  - New FreightCommon
5.3.1  - New FreightCommon
5.4  - FREIGHT-206 City field for FedEx LTL.
5.4.1  - New FreightCommon
5.4.2  - New WSA Logger
