<?php
//
$timeo_start = microtime(true);
ini_set("memory_limit","128M");

//==============================================================
//==============================================================
//==============================================================
include("mpdf.php");

$mpdf=new mPDF(); 
//$mpdf->debug = true;

$mpdf->useAdobeCJK = true;		// Default setting in config.php
						// You can set this to false if you have defined other CJK fonts

$mpdf->SetAutoFont(AUTOFONT_ALL);	//	AUTOFONT_CJK | AUTOFONT_THAIVIET | AUTOFONT_RTL | AUTOFONT_INDIC	// AUTOFONT_ALL
						// () = default ALL, 0 turns OFF (default initially)
$content = file_get_contents('china_visa_testForm.html');
//print_r($content);die;

$mpdf->WriteHTML($content);
$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================


?>
