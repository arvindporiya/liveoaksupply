<?php
$name='Journal';
$type='TTF';
$desc=array (
  'Ascent' => 954,
  'Descent' => -378,
  'CapHeight' => 954,
  'Flags' => 4,
  'FontBBox' => '[-1004 -378 1964 954]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 500,
);
$up=-133;
$ut=20;
$ttffile='/home/liveoaksupply/public_html/pdfcreation/ttfonts/journal-Regular.ttf';
$TTCfontID='0';
$originalsize=130956;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='journal';
$panose=' 0 0 0 0 4 0 0 0 0 0 0 0';
$haskerninfo=false;
$unAGlyphs=false;
?>