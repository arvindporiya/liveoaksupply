<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<TITLE>Credit Application</TITLE>
<link href="js/sign/jquery.signaturepad.css" rel="stylesheet">
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/validationEngine/validationEngine.jquery.css" type="text/css" />
<script type="text/javascript" src="js/validationEngine/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="js/validationEngine/jquery.validationEngine.js"></script>
<STYLE type="text/css">
.width-100{width:100%;display:block;}
input[type="text"]{
    border-top: none;
    border-right: none;
    border-bottom: 1px #000 solid;
    border-left: none;
	background-color: #dde4ff;
	padding:5px 0;
}
input[type="text"]:focus{
    background-color: #fff;
}
body {margin-top: 0px;margin-left: 0px;}
#page_1 {position:relative; overflow: hidden;margin: 24px auto 52px auto;padding: 0px;border: none;width: 1000px;}
#page_1 #dimg1 {position:absolute;top:0px;left:0px;z-index:-1;width:576px;height:713px;}
#page_1 #dimg1 #img1 {width:576px;height:713px;}
.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}
.ft0{font: bold 19px 'Arial';line-height: 22px;}
.ft1{font: 15px 'Arial';line-height: 19px;}
.ft2{font: bold 16px 'Arial';line-height: 19px;}
.ft3{font: bold 14px 'Arial';line-height: 16px;}
.ft4{font: 14px 'Arial';line-height: 14px;}
.ft5{font: 1px 'Arial';line-height: 1px;}
.ft6{font: 10px 'Arial';line-height: 13px;}
.ft7{font: 14px 'Arial';line-height: 15px;}
.ft8{font: 1px 'Arial';line-height: 10px;}
.ft9{font: 1px 'Arial';line-height: 11px;}
.ft10{font: 1px 'Arial';line-height: 14px;}
.ft11{font: bold 15px 'Arial';line-height: 15px;}
.ft12{font: bold 15px 'Arial';line-height: 12px;}
.ft13{font: 1px 'Arial';line-height: 12px;}
.ft14{font: bold 11px 'Arial';line-height: 14px;}
.ft15{font: bold 15px 'Arial';line-height: 14px;}
.ft16{font: 10px 'Arial';line-height: 12px;}
.ft17{font: bold 8px 'Arial';line-height: 12px;}
.p0{text-align: left;padding-left: 124px;margin-top: 62px;margin-bottom: 0px;}
.p1{text-align: left;padding-left: 134px;padding-right: 169px;margin-top: 0px;margin-bottom: 0px;text-indent: 43px;}
.p2 {
  margin-bottom: 0;
  margin-top: 0;
  text-align: center;
  width: 100%;
}
.p3 {
  margin-bottom: 0;
  margin-top: 9px;
  text-align: center;
}
.p3_A {
  margin-bottom: 0;
  margin-top: 9px;
  padding-left:8px;
  line-height:25px;
}
.p4{text-align: left;padding-left: 8px;margin-top: 10px;margin-bottom: 0px;}
.p5{text-align: left;padding-left: 8px;margin-top: 13px;margin-bottom: 0px;}
.p6{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 32px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: left;padding-left: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-left: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 78px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: left;padding-left: 9px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;padding-left: 53px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 8px;margin-top: 6px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 8px;margin-top: 0px;margin-bottom: 0px;width:100%;float:left;padding-bottom:10px;}
.p19{text-align: left;padding-left: 8px;margin-top: 11px;margin-bottom: 0px;}
.p20{text-align: left;padding-left: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: left;padding-left: 51px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: left;padding-left: 34px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: left;padding-left: 39px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: left;padding-left: 73px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p25{text-align: left;padding-left: 115px;margin-top: 0px;margin-bottom: 0px;}
.p26{text-align: left;padding-left: 8px;padding-right: 45px;margin-top: 11px;margin-bottom: 0px;}
.p27{text-align: left;padding-left: 8px;margin-top: 22px;margin-bottom: 0px;}
.p28{text-align: justify;padding-left: 8px;padding-right: 63px;margin-top: 11px;margin-bottom: 0px;}
.p29{text-align: left;padding-left: 8px;margin-top: 23px;margin-bottom: 0px;}

.td0{padding: 0px;margin: 0px;width: 160px;vertical-align: bottom;}
.td1{padding: 0px;margin: 0px;width: 14px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 9px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 60px;vertical-align: bottom;}
.td4{padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
.td5{padding: 0px;margin: 0px;width: 13px;vertical-align: bottom;}
.td6{padding: 0px;margin: 0px;width: 205px;vertical-align: bottom;}
.td7{padding: 0px;margin: 0px;width: 11px;vertical-align: bottom;}
.td8{padding: 0px;margin: 0px;width: 31px;vertical-align: bottom;}
.td9{padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
.td10{padding: 0px;margin: 0px;width: 17px;vertical-align: bottom;}
.td11{padding: 0px;margin: 0px;width: 80px;vertical-align: bottom;}
.td12{padding: 0px;margin: 0px;width: 586px;vertical-align: bottom;}
.td13{padding: 0px;margin: 0px;width: 16px;vertical-align: bottom;}
.td14{padding: 0px;margin: 0px;width: 37px;vertical-align: bottom;}
.td15{padding: 0px;margin: 0px;width: 17px;vertical-align: bottom;}
.td16{padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
.td17{padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
.td18{padding: 0px;margin: 0px;width: 369px;vertical-align: bottom;}
.td19{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 184px;vertical-align: bottom;}
.td20{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 22px;vertical-align: bottom;}
.td21{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 11px;vertical-align: bottom;}
.td22{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 15px;vertical-align: bottom;}
.td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 5px;vertical-align: bottom;}
.td24{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 80px;vertical-align: bottom;}
.td25{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 183px;vertical-align: bottom;}
.td26{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 61px;vertical-align: bottom;}
.td27{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 122px;vertical-align: bottom;}
.td28{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 183px;vertical-align: bottom;}
.td29{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 133px;vertical-align: bottom;}
.td30{border-left: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 160px;vertical-align: bottom;}
.td31{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 8px;vertical-align: bottom;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 12px;vertical-align: bottom;}
.td33{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 79px;vertical-align: bottom;}
.td34{border-left: #000000 1px solid;border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: bottom;}
.td35{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 145px;vertical-align: bottom;}
.td36{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 109px;vertical-align: bottom;}
.td37{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td38{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 222px;vertical-align: bottom;}
.td39{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: bottom;}
.td40{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 145px;vertical-align: bottom;}
.td41{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 109px;vertical-align: bottom;}
.td42{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 107px;vertical-align: bottom;}
.td43{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 222px;vertical-align: bottom;}

.tr0{height: 19px;}
.tr1{height: 17px;}
.tr2{height: 26px;}
.tr3{height: 10px;}
.tr4{height: 11px;}
.tr5{height: 14px;}
.tr6{height: 15px;}
.tr7{height: 28px;}
.tr8{height: 12px;}
.tr9{height: 27px;}

.t0{width: 736px;margin-top: 10px;font: bold 12px 'Arial';}
.t1{width: 735px;font: bold 12px 'Arial';}
th{padding:5px 0 !important;}
th p {padding: 0 !important;text-align: center !important;}
.table{padding:7px;}
.typeIt .current, .drawIt .current {
  background: #ccc none repeat scroll 0 0;
  border-radius: 0 0 10px 10px;
  padding: 1px 10px 2px;
}
.sign_note{
  left: 52px;
  margin-top: 27px;
  position: absolute;
}
.header{width:100%;text-align:center;float:left;}
.line{width:100%;text-align:center;float:left;}
</STYLE>
</HEAD>

<BODY>

<form action="processForm.php" method="post" id="form">

<DIV id="page_1" >

<DIV class="header">
	<IMG src="logo.jpg" id="img1">
    <div class="line"><strong>“Where Quality Products Meet Excellent Service” Since 1986</strong></div>
    <div class="line">4225 Steve Reynolds Boulevard • Norcross, Georgia 30093</div>
    <div class="line">Phone: (770) <NOBR>963-3000</NOBR> • Fax: (770) <NOBR>963-3136</NOBR> • www.liveoaksupply.com</div>
</DIV>
<P class="p2 ft2">__________________________________________________________________________</P>
<P class="p3 ft3"><SPAN class="ft0">A</SPAN>PPLICATION FOR <SPAN class="ft0">C</SPAN>REDIT</P>
<P class="p3_A ft3">This page is encrypted with SSL for security purposes.<br />

We will only use the information below for purposes of obtaining credit. We will not sell, distribute or lease your personal information to third parties unless required by law to do so. <br />

Directions: please complete the entire application below and click the submit button at the bottom of the page.</P>
<P class="p4 ft4">Company Name:<input type="text" class="validate[required]" name="compnay_name" id="compnay_name" style="width:89%;"/></P>
<P class="p5 ft4">Physical Address: <input type="text" class="validate[required]" name="physical_address" id="physical_address" style="width:36%;"/> Mailing Address: <input type="text" class="validate[required]" name="mailing_address" id="mailing_address"  style="width:40%;"/></P>
<P class="p5 ft4">Telephone #: <input type="text" class="[validate[required],custom[phone]]" name="telephone" id="telephone" style="width:39.5%;"/> Fax #: <input type="text" class="[validate[required],custom[phone]]" name="fax" id="fax" style="width:46.5%;"/></P>
<P class="p5 ft4">City/State/Zip: <input type="text" class="validate[required]" name="city_state_zip" id="city_state_zip" style="width:40%;"/> Website: <input type="text"  name="website" id="website" style="width:43.5%;"/></P>
<P class="p5 ft4">Credit Limit Requested: <input type="text" class="validate[required]" name="credit_limit" id="credit_limit" style="width:16%;"/> Date Business Opened: <input type="text" class="validate[required]" name="business_opened" id="business_opened" style="width:17%;"/> Social Security or EIN Number: <input type="text" class="validate[required]" name="ein" id="ein" style="width:15%;"/></P>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr0 td0"><P class="p6 ft4">Type of Business: Corporation</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="checkbox" class="validate[minCheckbox[1]]" name="type_of_business[]" id="type_of_business1"  value="Corporation" /></P></TD>
	<TD class="tr0 td2"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr0 td3"><P class="p8 ft4">Partnership</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="checkbox" class="validate[minCheckbox[1]]" name="type_of_business[]" id="type_of_business2" value="Partnership" /></P></TD>
	<TD class="tr0 td4"><P class="p9 ft4">Proprietorship</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="checkbox" class="validate[minCheckbox[1]]" name="type_of_business[]" id="type_of_business3" value="Proprietorship" /></P></TD>
	<TD class="tr0 td5"><P class="p7 ft5">&nbsp;</P></TD>
	<TD colspan=2 class="tr0 td6"><P class="p10 ft6">Are purchase orders required? Yes</P></TD>
	<TD class="tr1 td7"><P class="p7 ft5">&nbsp;<input type="radio" class="validate[required]" name="purchase_orders" id="purchase_orders" value="Yes"/></P></TD>
	<TD colspan=2 class="tr0 td8"><P class="p11 ft7">No</P></TD>
	<TD class="tr1 td1"><P class="p7 ft5">&nbsp;<input type="radio" class="validate[required]" name="purchase_orders" id="purchase_orders" value="No"/></P></TD>
	<TD class="tr0 td9"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr0 td10"><P class="p7 ft5">&nbsp;</P></TD>
	<TD class="tr0 td11"><P class="p7 ft5">&nbsp;</P></TD>
</TR>
<TR>
	<TD colspan=11 rowspan=2 class="tr2 td12"><P class="p6 ft4">Have you ever filed for bankruptcy (answer yes even if the bankruptcy was filed under another company name)? Yes</P></TD>
	<TD class="tr3 td13"><P class="p7 ft8">&nbsp;</TD>
	<TD colspan=3 rowspan=2 class="tr2 td14"><P class="p12 ft7">No</P></TD>
	<TD class="tr3 td15"><P class="p7 ft8">&nbsp;</P></TD>
	<TD class="tr4 td11"><P class="p7 ft9">&nbsp;</P></TD>
</TR>
<TR>
	<TD class="tr5 td16"><P class="p7 ft10">&nbsp;<input type="radio" class="validate[required]" name="filed_for_bankruptcy" id="filed_for_bankruptcy" value="Yes"/></P></TD>
	<TD class="tr5 td17"><P class="p7 ft10">&nbsp;<input type="radio" class="validate[required]" name="filed_for_bankruptcy" id="filed_for_bankruptcy" value="No"/></P></TD>
	<TD class="tr6 td11"><P class="p7 ft5">&nbsp;</P></TD>
</TR>
</table>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD colspan=8 class="tr7 td18"><P class="p6 ft11">Name of President and Treasurer, Owner(s) or Partners:</P></TD>
</TR>
</table>
<TABLE cellpadding=0 cellspacing=0 width="100%" class="table">
<TR>
	<th class="tr5 td34"><P class="p20 ft15">Name</P></th>
	<th class="tr5 td35"><P class="p21 ft15">Full Address</P></th>
	<th class="tr5 td36"><P class="p22 ft15">Direct Phone #</P></th>
	<th class="tr5 td37"><P class="p23 ft15">Email Address</P></th>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<input type="text" class="validate[required]" name="president_name1" id="president_name1" style="width:99%;border:none;"/></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<input type="text" class="validate[required]" name="president_address1" id="president_address1" style="width:99%;border:none;"/></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<input type="text" class="[validate[required],custom[phone]]" name="president_direct_phone1" id="president_direct_phone1" style="width:99%;border:none;"/></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<input type="text" class="[validate[required],custom[email]]" name="president_email1" id="president_email1" style="width:99%;border:none;"/></P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<input type="text" class="" name="president_name2" id="president_name2" style="width:99%;border:none;"/></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<input type="text" class="" name="president_address2" id="president_address2" style="width:99%;border:none;"/></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<input type="text" class="validate[custom[phone]]" name="president_direct_phone2" id="president_direct_phone2" style="width:99%;border:none;"/></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<input type="text" class="validate[custom[email]]" name="president_email2" id="president_email2" style="width:99%;border:none;"/></P></TD>
</TR>

</TABLE>
<P class="p17 ft4">A/P Contact: <input type="text" class="validate[required]" name="ap_contact" id="ap_contact" style="width:41%;"/> Title: <input type="text" class="validate[required]" name="ap_title" id="ap_title" style="width:46%;"/></P>
<P class="p5 ft4">Telephone #: <input type="text" class="[validate[required],custom[phone]]" name="ap_telephone" id="ap_telephone" style="width:40.5%;"/> Email Address: <input type="text" class="[validate[required],custom[email]]" name="ap_email" id="ap_email" style="width:40%;"/></P>
<P class="p5 ft11">List bank account(s):</P>
<P class="p5 ft14"><input type="text" class="validate[required]" name="bank_ref" id="bank_ref" style="width:23%;"/><input type="text" class="validate[required]" name="account_no" id="account_no" style="width:23%;"/><input type="text" class="validate[required]" name="bank_address" id="bank_address" style="width:33%;"/><input type="text" class="[validate[required],custom[phone]]" name="bank_telephone" id="bank_telephone" style="width:20%;"/></P>
<P class="p18 ft7"><span style="width:27%;float: left;">Bank Reference & Account Holder </span><span style="width:19%;float: left;">Account #</span><span style="width:33%;float: left;">Full Address</span><span style="width:21%;float: left;">Telephone #</span></P>
<P class="p19 ft11">Please list all business/trade References (this MUST be businesses in which you purchase tools and/or materials, NOT subcontractors):</P>
<TABLE cellpadding=0 cellspacing=0 width="100%" class="table">
<TR>
	<th class="tr5 td34"><P class="p20 ft15">Trade Reference Name</P></th>
	<th class="tr5 td35"><P class="p21 ft15">Full Address</P></th>
	<th class="tr5 td36"><P class="p22 ft15">Phone #</P></th>
	<th class="tr5 td37"><P class="p23 ft15">Fax #</P></th>
	<th class="tr5 td38"><P class="p24 ft15">Email Address</P></th>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<input type="text" name="trade_ref_no1" id="trade_ref_no1" class="validate[required]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<input type="text" name="trade_address1" id="trade_address1" class="validate[required]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<input type="text" name="trade_phone1" id="trade_phone1" class="[validate[required],custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<input type="text" name="trade_fax1" id="trade_fax1" class="[validate[required],custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td43"><P class="p7 ft5">&nbsp;<input type="text" name="trade_email1" id="trade_email1" class="[validate[required],custom[email]]" style="width:99%;border:none;" /></P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<input type="text" name="trade_ref_no2" id="trade_ref_no2" class="" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<input type="text" name="trade_address2" id="trade_address2" class="" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<input type="text" name="trade_phone2" id="trade_phone2" class="validate[custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<input type="text" name="trade_fax2" id="trade_fax2" class="validate[custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td43"><P class="p7 ft5">&nbsp;<input type="text" name="trade_email2" id="trade_email2" class="validate[custom[email]]" style="width:99%;border:none;" /></P></TD>
</TR>
<TR>
	<TD class="tr9 td39"><P class="p7 ft5">&nbsp;<input type="text" name="trade_ref_no3" id="trade_ref_no3" class="" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td40"><P class="p7 ft5">&nbsp;<input type="text" name="trade_address3" id="trade_address3" class="" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td41"><P class="p7 ft5">&nbsp;<input type="text" name="trade_phone3" id="trade_phone3" class="validate[custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td42"><P class="p7 ft5">&nbsp;<input type="text" name="trade_fax3" id="trade_fax3" class="validate[custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr9 td43"><P class="p7 ft5">&nbsp;<input type="text" name="trade_email3" id="trade_email3" class="validate[custom[email]]" style="width:99%;border:none;"  /></P></TD>
</TR>
<TR>
	<TD class="tr2 td39"><P class="p7 ft5">&nbsp;<input type="text" name="trade_ref_no4" id="trade_ref_no4" class="" style="width:99%;border:none;" /></P></TD>
	<TD class="tr2 td40"><P class="p7 ft5">&nbsp;<input type="text" name="trade_address4" id="trade_address4" class="" style="width:99%;border:none;" /></P></TD>
	<TD class="tr2 td41"><P class="p7 ft5">&nbsp;<input type="text" name="trade_phone4" id="trade_phone4" class="validate[custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr2 td42"><P class="p7 ft5">&nbsp;<input type="text" name="trade_fax4" id="trade_fax4" class="validate[custom[phone]]" style="width:99%;border:none;" /></P></TD>
	<TD class="tr2 td43"><P class="p7 ft5">&nbsp;<input type="text" name="trade_email4" id="trade_email4" class="validate[custom[email]]" style="width:99%;border:none;"  /></P></TD>
</TR>
</TABLE>
<P class="p25 ft11">**PLEASE NOTE: HD SUPPLY/WHITE CAP, ARGOS, LAFARGE & UNITED RENTAL WILL NOT QUALIFY**</P>
<P class="p26 ft16">Credit terms are 30 days from date of invoice. Outstanding balances are subject to 1.5% per month interest. The undersigned authorizes and releases all banks, persons, and companies listed on this application to furnish information and authorize the checking of credit. The undersigned agrees to pay all collection costs, court costs, and legal fees incurred to collect delinquent balances.</P>
<P class="p27 ft4 sigPad_2"><input type="text" class="validate[required] name" name="print_name" id="print_name" style="width:25%;float:left;"/><input type="text" class="validate[required]" name="print_title" id="print_title" style="width:25%;float:left;"/><span style="float: left; width: 35%;margin-top:-6px;"><span class=""><span class="typed"></span><span class="sig sigWrapper current"><canvas height="31" width="344" class="pad"></canvas><br /><span class="typeIt" style="margin-right:10px;"><a href="#type-it" class="current">Type It</a></span><span class="drawIt" style="margin-right:10px;"><a href="#draw-it" >Draw It</a></span><span class="clearButton"><a href="#clear">Clear</a></span><input type="hidden" class="output" id="output_2" name="output_2" value=""></span></span></span><input type="text" class="validate[required]" name="signature_date" id="signature_date" style="width:15%;float:left;"/></P>
<P class="p18 ft7"><span style="width:25%;float: left;">Print Name<small> (use for type it signature)</small></span><span style="width:25%;float: left;">Title</span><span style="width:34%;float: left;">Signature</span><span style="width:15%;float: left;">Date</span></P>
<P class="p28 ft16" style="clear:both"><SPAN class="ft17">PERSONAL GUARANTY</SPAN>: In consideration of credit being extended to the above named firm, I personally guarantee all indebtedness hereunder. I further agree that this guarantee is an absolute, complete and continuing one and no notice of indebtedness or any extension of credit already or hereafter contracted by or extended need be given. The terms may be rearranged, extended and/or renewed without notice to me. I will, within five days from date of notice that the account is past due, pay the amount due.</P>
<P class="p29 ft4 sigPad_1"><span class="sign_note"><small> (use for type it signature)</small></span>Name: <input type="text" class="validate[required] name" name="name" id="name" style="width:25.5%;"/> SSN: <input type="text" class="validate[required]" name="ssn" id="ssn" style="width:24%;"/>Signature:<span style="float: right; width: 35%;margin-top:-6px;"><span class=""><span class="typed"></span><span class="sig sigWrapper current"><canvas height="31" width="344" class="pad"></canvas><br /><span class="typeIt" style="margin-right:10px;"><a href="#type-it" class="current">Type It</a></span><span class="drawIt" style="margin-right:10px;"><a href="#draw-it" >Draw It</a></span><span class="clearButton"><a href="#clear">Clear</a></span><input type="hidden" class="output" id="output_2" name="output_2" value=""></span></span></span></P>
<P class="p5 ft4" style="clear:both">Home Address: <input type="text" class="validate[required]" name="home_address" id="home_address" style="width:87%;"/></P>
<P class="p5 ft4">Direct Phone #: <input type="text" class="[validate[required],custom[phone]]" name="direct_phone" id="direct_phone" style="width:24.5%;"/> Driver’s License #: <input type="text" class="validate[required]" name="license_no" id="license_no" style="width:24%;"/> Date: <input type="text" class="validate[required]" name="date" id="date" style="width:24%;"></P>
	<DIV style="width:100%;text-align:center;margin-top:10px;"><input type="image" src="submit_btn.png" /></DIV>
</DIV>
	<input type="hidden" name="action" value="submit" />
</form>
<!--<form method="post" action="" class="sigPad">
    <label for="name">Print your name</label>
    <input type="text" name="name" id="name" class="name">
    <p class="typeItDesc">Review your signature</p>
    <p class="drawItDesc">Draw your signature</p>
    <ul class="sigNav">
      <li class="typeIt"><a href="#type-it" class="current">Type It</a></li>
      <li class="drawIt"><a href="#draw-it" >Draw It</a></li>
      <li class="clearButton"><a href="#clear">Clear</a></li>
    </ul>
    <div class="sig sigWrapper">
      <div class="typed"></div>
      <canvas class="pad" width="198" height="55"></canvas>
      <input type="hidden" name="output" class="output">
    </div>
    <button type="submit">I accept the terms of this agreement.</button>
  </form>-->
<script type="text/javascript">

	jQuery(document).ready(function(){

		jQuery("#form").validationEngine();

	});
</script>
<script src="js/sign/jquery.signaturepad.js"></script>
  <script>
    jQuery(document).ready(function() {
	var sig = '';	


      //jQuery('#ChronoContact_PaymentOptions').signaturePad({drawOnly:true});
		//jQuery('.sigPad_1').signaturePad({drawOnly:true});
		jQuery('.sigPad_1').signaturePad();
		jQuery('.sigPad_2').signaturePad();
		 //jQuery('.drawIt a').click();
	   jQuery('.sigPad').signaturePad({displayOnly:true,lineColour : 'blue'});
	  /* var sig = '<?php echo $details->output_application?>';
		jQuery('.sigPad_1').signaturePad({displayOnly:true}).regenerate(sig);*/
    });
  </script>
</BODY>
</HTML>