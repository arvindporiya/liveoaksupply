<?php
ini_set('max_execution_time', 3600);
//ini_set('memory_limit', -1);

require_once('app/Mage.php');
Mage::app();

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$product = Mage::getModel("catalog/product");
	$product
		->setStoreId(0) 
		->setWebsiteIds(array(1)) 
		->setAttributeSetId(4) 
		->setTypeId('simple') 
		->setCreatedAt(strtotime('now')) 
		->setSku('my-product'.'-'.strtotime('now'))
		->setName('My Product'.'-'.strtotime('now'))
		->setStatus(1)
		->setWeight(0)
		->setVisibility(1)            
		->setPrice(11.22)
		->setTaxClassId(0)
		->setDescription('This is a long description')
		->setShortDescription('This is a short description')
		->setStockData(array(
			   'use_config_manage_stock' => 0,
			   'manage_stock'=>1,
			   'min_sale_qty'=>1,
			   'max_sale_qty'=>1,
			   'is_in_stock' => 1,
			   'qty' => 1
			)
		)
		->setCategoryIds(array(159));           
	//$product->save();
	//$productId = $product->getId();
	//echo $productId;
	
	//Add Product to Cart
	$id = '5567'; // Replace id with your product id
	$qty = '1'; // Replace qty with your qty
	$params = array(
    'product' => 5567,
    'qty' => 1);
 
$cart = Mage::getSingleton('checkout/cart');
 
$product = new Mage_Catalog_Model_Product();
$product->load(5567);
$product->getData();
$cart->addProduct($product, $params);
$cart->save();
 
Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
 
$message = $this->__('Custom message: %s was successfully added to your shopping cart.', $product->getName());
Mage::getSingleton('checkout/session')->addSuccess($message);
?>