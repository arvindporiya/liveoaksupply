<?php
function csv_to_array($filename='', $delimiter=',')
{
	//print_r($_FILES);die;
	if(!file_exists($filename) || !is_readable($filename))
		return FALSE;	
	$header = NULL;
	$dataArray = array();
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename='.$_FILES['file']['name']);
	// create a file pointer connected to the output stream
	$output = fopen('php://output', 'w');
	// output the column headings
	fputcsv($output, array('Code','Country','State','Zip/Post Code','Rate','Zip/Post is Range','Range From','Range To'));	
	if (($handle = fopen($filename, 'r')) !== FALSE)
	{
		while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
		{
			
			if(!$header){
				$header = $row;
			}else{
				//$dataArray[] = (object)array_combine($header, $row);
				$data = (object)array_combine($header, $row);
				//echo $data->State;
				//=CONCATENATE("US-NC-",B2,"-Rate")
				//$row =new stdClass();
				$row = array();
				$row['Code'] = "US-".$data->State."-".$data->ZipCode."-Rate";
				$row['Country'] = 'US';
				$row['State'] = $data->State;
				$row['Zip/Post Code'] = $data->ZipCode;
				$row['Rate'] = $data->EstimatedCombinedRate;
				$row['Zip/Post is Range'] = 0;
				$row['Range From'] = '';
				$row['Range To'] = '';
				$row_insert = (object)$row;
				//echo "US-".$data->State."-".$data->ZipCode."-Rate"."<br />";
				fputcsv($output, $row);
			}
		}
		//fclose($handle);
	}
	die;
	header('Location:formatCSV.php?msg=done');
	//return $dataArray;
}
/**
 * Example
 */
if($_POST['action']=='submit'){ 
	csv_to_array($_FILES['file']['tmp_name']);
}
$error = '';
if($_POST['action']=='login'){
	   if(($_POST['username'] == "stefano" && $_POST['password'] == "sfg6678$$") || ($_POST['username'] == "nick" && $_POST['password'] == "EvergreenRezdo2088") || ($_POST['username'] == "doug" && $_POST['password'] == "EvergreenRezdo2088")){
			$_SESSION['adminusername']=$_POST['username'];		  
		}else{
			$error="Invalid username or password";
		}
 }
 if($_POST['action']=='logout'){
	  $_SESSION['adminusername'] = '';
      unset($_SESSION['adminusername']);
 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Import Event into Rezdo</title>
</head>

<body>
<?php if(isset($_SESSION['adminusername']) || 1==1){?>
<form action="formatCSV.php" method="post" id="formlogout">
    <input type="hidden" name="action" value="logout" />
    <input type="submit"  name="button" value="Logout" />
  </div>
</form>
<form action="formatCSV.php" method="post" enctype="multipart/form-data" id="form">
  <div style="width:50%;margin:10% auto 0 auto;">
    <?php if(isset($_GET['msg']) && $_GET['msg'] == 'done'):?>
    <h1 style="color:green">Events Imported successfully into database.</h1>
    <?php endif?>
    <label>Please Upload CSV file</label>
    <input type="file" name="file" id="file" onChange="load_image(this.id,this.value)"/>
    <input type="hidden" name="action" value="submit" />
    <input type="submit"  name="button" value="Submit" />
  </div>
</form>
<script type="text/javascript">
	function formSubmit(){
		alert(document.getElementById('file').value);
	}
	function load_image(id,ext){
		if(validateExtension(ext) == false){
			alert("upload only CSV format ");
			document.getElementById(id).value='';
			document.getElementById(id).focus();
			return;
		}
	}
	function validateExtension(v){
		//var allowedExtensions = new Array("jpg","JPG","jpeg","JPEG","gif","GIF","png","PNG","pdf");
		var allowedExtensions = new Array("csv");
		for(var ct=0;ct<allowedExtensions.length;ct++){
			sample = v.lastIndexOf(allowedExtensions[ct]);
			if(sample != -1){return true;}
		}
		return false;
	}
</script>
<?php }else{?>

<table width="20%" style="margin:0 auto;padding-top:70px;" border="0" cellspacing="0" cellpadding="0">
  <form name="frm_admin_login" method="post" id="frm_admin_login" action="formatCSV.php">
    <input type="hidden" name="action" id="action" value="login" />
   <tr>
   <?php if($error!=""){?>
    	<td  colspan="2"  height="10" style="color:red;"><?php echo $error;?></td>
    <?php }else{?>
    	
    <?php }?>
    </tr>
    <tr>
      <td width="30%" height="25" class="normalTxt"><strong>Username</strong></td>
      <td width="70%" class="normalTxt"><input name="username" id="username" type="text" class="input1" value="<?php echo $_POST['username'];?>" ></td>
    </tr>
    <tr>
      <td height="25" class="normalTxt"><strong>Password</strong></td>
      <td class="normalTxt"><input name="password" type="password" id="password" class="input1" value="<?php echo $_POST['password'];?>" ></td>
    </tr>
    <tr>
      <td class="normalTxt">&nbsp;</td>
      <td height="35" class="normalTxt"><input type="button" onclick="formSubmit();" value="Login" name="submit"></td>
    </tr>
    <input type="submit" style="display:none;" id="submitBtn" />
  </form>
</table>
<script type="text/javascript">
	function formSubmit(){
		if(document.getElementById('username').value == ''){
			alert("Please insert username.");
		}else if(document.getElementById('password').value == ''){
			alert("Please insert password.");
		}else{
			//document.getElementById("frm_admin_login").submit();
			document.getElementById('submitBtn').click();
		}
	}
</script>
<?php }?>
</body>
</html>