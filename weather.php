<?php
/* 
function getHtml($url, $post = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if(!empty($post)) {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    } 
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}
$html = getHtml('//weather.weatherbug.com/desktop-weather/web-widgets/getSticker.html?ZipCode=30301&ZCode=z5545&Size=125x125&StationID=ATCH1&units=0&Version=2&lang_id=en-us');
print_r($html);
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Weather</title>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="/weatherApi/jquery.simpleWeather.js" type="text/javascript" ></script>
</head>

<body>
<style type="text/css">
/*
  Docs at http://http://simpleweatherjs.com

  Look inspired by http://www.degreees.com/
  Used for demo purposes.

  Weather icon font from http://fonts.artill.de/collection/artill-weather-icons

  DO NOT hotlink the assets/font included in this demo. If you wish to use the same font icon then download it to your local assets at the link above. If you use the links below odds are at some point they will be removed and your version will break.
*/

@font-face {
    font-family: 'weather';
    src: url('/weatherApi/artill_clean_icons-webfont.eot');
    src: url('/weatherApi/artill_clean_icons-webfont.eot?#iefix') format('embedded-opentype'),
         url('/weatherApi/artill_clean_icons-webfont.woff') format('woff'),
         url('/weatherApi/artill_clean_icons-webfont.ttf') format('truetype'),
         url('https://s3-us-west-2.amazonaws.com/s.cdpn.io/93/artill_clean_icons-webfont.svg#artill_clean_weather_iconsRg') format('svg');
    font-weight: normal;
    font-style: normal;
}

html {
  width: 100%;
  height: 100%;
  background: #fff;
  background-size: cover;
}

body {
  padding: 0 0;
  font: 13px 'Open Sans', "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
}

#weather {
  float: left;
  margin: 0 auto;
  text-align: center;
  text-transform: uppercase;
  width: 100%;
}

i {
  color: #000;
  font-family: weather;
  font-size: 28px;
  font-style: normal;
  font-weight: normal;
  line-height: 1;
}

.icon-0:before { content: ":"; }
.icon-1:before { content: "p"; }
.icon-2:before { content: "S"; }
.icon-3:before { content: "Q"; }
.icon-4:before { content: "S"; }
.icon-5:before { content: "W"; }
.icon-6:before { content: "W"; }
.icon-7:before { content: "W"; }
.icon-8:before { content: "W"; }
.icon-9:before { content: "I"; }
.icon-10:before { content: "W"; }
.icon-11:before { content: "I"; }
.icon-12:before { content: "I"; }
.icon-13:before { content: "I"; }
.icon-14:before { content: "I"; }
.icon-15:before { content: "W"; }
.icon-16:before { content: "I"; }
.icon-17:before { content: "W"; }
.icon-18:before { content: "U"; }
.icon-19:before { content: "Z"; }
.icon-20:before { content: "Z"; }
.icon-21:before { content: "Z"; }
.icon-22:before { content: "Z"; }
.icon-23:before { content: "Z"; }
.icon-24:before { content: "E"; }
.icon-25:before { content: "E"; }
.icon-26:before { content: "3"; }
.icon-27:before { content: "a"; }
.icon-28:before { content: "A"; }
.icon-29:before { content: "a"; }
.icon-30:before { content: "A"; }
.icon-31:before { content: "6"; }
.icon-32:before { content: "1"; }
.icon-33:before { content: "6"; }
.icon-34:before { content: "1"; }
.icon-35:before { content: "W"; }
.icon-36:before { content: "1"; }
.icon-37:before { content: "S"; }
.icon-38:before { content: "S"; }
.icon-39:before { content: "S"; }
.icon-40:before { content: "M"; }
.icon-41:before { content: "W"; }
.icon-42:before { content: "I"; }
.icon-43:before { content: "W"; }
.icon-44:before { content: "a"; }
.icon-45:before { content: "S"; }
.icon-46:before { content: "U"; }
.icon-47:before { content: "S"; }

#weather h2 {
  clear: both;
  color: #000;
  float: left;
  font-size: 25px;
  font-weight: 300;
  margin: 0 0 8px;
  text-align: center;
  text-shadow: 0 1px 3px rgba(0, 0, 0, 0.15);
  width: 100%;
  padding-top:10px;
}

#weather ul {
  clear: both;
  float: left;
  margin: 0 0 10px;
  padding: 0;
  text-align: center;
  width: 100%;
}

#weather li {
  background: #fff;
  background: rgba(255,255,255,0.90);
  padding: 0;
  display: inline-block;
  border-radius: 5px;
}

#weather .currently {
  margin: 0 20px;
}
</style>

<!-- Docs at http://http://simpleweatherjs.com -->
<div id="weather"></div>
<script type="text/javascript">
// Docs at http://simpleweatherjs.com
$(document).ready(function() {
  $.simpleWeather({
    location: 'Atlanta, GA',
    woeid: '',
    unit: 'f',
    success: function(weather) {
      html = '';
      html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
      //html += '<li class="currently">'+weather.currently+'</li>';
      //html += '<li>'+weather.wind.direction+' '+weather.wind.speed+' '+weather.units.speed+'</li></ul>';
	  html += '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
  
      $("#weather").html(html);
    },
    error: function(error) {
      $("#weather").html('<p>'+error+'</p>');
    }
  });
});

</script>
</body>
</html>
