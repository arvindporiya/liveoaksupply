<?php
require_once 'app/Mage.php';
Mage::app('default'); // Default or your store view name.
function is_youtube($url)
{
	return (preg_match('/youtu\.be/i', $url) || preg_match('/youtube\.com\/watch/i', $url));
}

/**
 * This function checks url has vimeo
 * @param type $url
 * @return type
 */
function is_vimeo($url)
{
	return (preg_match('/vimeo\.com/i', $url));
}

/**
 * This function get the youtube id.
 * @param type $url
 * @return string
 */
function youtube_video_id($url)
{
	$pattern = '/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/';
	preg_match($pattern, $url, $matches);
	if (count($matches) && strlen($matches[7]) == 11)
	{
		return $matches[7];
	}
}

/**
 * This function get the vimeo id
 * @param type $url
 * @return string
 */
function vimeo_video_id($url)
{
	$pattern = '/\/\/(www\.)?vimeo.com\/(\d+)($|\/)/';
	preg_match($pattern, $url, $matches);
	if (count($matches))
	{
		return $matches[2];
	}
	
	return '';
}
if($_REQUEST['request'] == 'getTitle'){
	$productid = $_REQUEST['id'];
	$title = Mage::getModel('catalog/product')->load($productid)->getName();
	$short_description = Mage::getModel('catalog/product')->load($productid)->getShortDescription();
	$description = Mage::getModel('catalog/product')->load($productid)->getDescription();
	echo '<div class="product-name" >
                  <h1>'.$title.'</h1>
                </div>
                <div class="short-description">
                  <div class="std">'.$short_description.'</div>
                </div>';
	exit;			
}
//get a new category object
if($_REQUEST['request'] == 'getVideo'){
	$str = $_REQUEST['str'];
	$category = $_REQUEST['category'];
	//$products = Mage::getModel('catalog/product')->getCollection()->addFilter('sku','p1');
	$products = Mage::getModel('catalog/product')->getCollection();
	$products->addAttributeToFilter('visibility', array('eq' => '4'));
	$products->addAttributeToFilter('status', array('eq' => '1'));
	if($str !=""){
		$products->addAttributeToFilter(array(
				array(
					'attribute' => 'name',
					'like' => '%'.$str.'%'),
				array(
					'attribute' => 'sku',
					'like' => '%'.$str.'%')	
			));
	}
	if($category !=""){
		$products->joinField('category_id','catalog/category_product','category_id','product_id=entity_id',null,'left');
		$products->addAttributeToFilter('category_id', array('in' => array($category)));
	}		
	$productVideos = array();
	foreach($products as $prod) {
		$product = Mage::getModel('catalog/product')->load($prod->getId());
		for($i = 1 ; $i <= 3 ; $i++){
			if($product->getData('video'.$i) !=""){
				$temp = array();
				$temp['id'] = $prod->getId();
				$temp['video'] = $product->getData('video'.$i);
				array_push($productVideos,$temp);
			}
		}
	}
	$a=0;
  $showThumbnail = false;

  if($productVideos && count($productVideos) >= 1){
	$showThumbnail = true;
  }
 ?>
 <?php if($productVideos && count($productVideos) > 0){?>
<?php $i = 0;$load_video='';$thumb_str=""; 
	foreach($productVideos as $row) :  ?>
<?php
			$videourl = $row['video'];
			$title = Mage::getModel('catalog/product')->load($row['id'])->getName();
			$short_description = Mage::getModel('catalog/product')->load($row['id'])->getShortDescription();
			$description = Mage::getModel('catalog/product')->load($row['id'])->getDescription(); 
            if($videourl !=""){
				if(is_youtube($videourl)){
					$video_code = youtube_video_id($videourl);
					$final_video_url = "http://www.youtube.com/embed/$video_code?rel=0";
					$final_thumnail_url = "http://img.youtube.com/vi/$video_code/mqdefault.jpg";
					if($content=file_get_contents("http://youtube.com/get_video_info?video_id=".$video_code)) {
						parse_str($content, $ytarr);
						$video_title=$ytarr['title'];
					} 
				}
				elseif(is_vimeo($videourl)) {
					$video_code = vimeo_video_id($videourl);
					$final_video_url = "http://player.vimeo.com/video/$video_code?title=0&byline=0&portrait=0;loop=0";
					$hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$video_code.php"));
					$final_thumnail_url = $hash[0]['thumbnail_medium'];
					$video_title = '';  
				}  
				if ($showThumbnail) {  
					$thumb_str .="<li><a href=$final_video_url target=\"productVideoFrame\" onclick='updateTitle(\"".$row['id']."\");'><img src=$final_thumnail_url height=\"110\" width=\"196\"><br /><br /><center>$video_title</center></a></li>";
				} 
				if($i==0){
					$load_video = $final_video_url;
					$load_video_thumnail = $final_thumnail_url;
				}
				$i++; 
            }
            ?>
<?php endforeach; ?>
<table width="100%" border="0" >
  <tr>
    <td valign="top">
	  <?php if($load_video !="" ){?>
      	<?php echo "<iframe name=\"productVideoFrame\" src=$load_video width=\"560\" height=\"340\" frameborder=\"0\"></iframe>";?>
      <?php }?>
    </td>
    <td id="product_detail">
        <div class="product-name" >
          <h1><?php echo $title;?></h1>
        </div>
        <div class="short-description">
          <div class="std"><?php echo $short_description;?></div>
        </div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><ul class='video_thumb'>
        <?php echo $thumb_str;?>
      </ul></td>
  </tr>
  
</table>
<?php }else{?>
	<table width="100%" border="0" >
  <tr>
    <td valign="top"><h3>No Product's Video Found. Please try again!</h3></td>
  </tr>
</table>
<?php }?>
<?php
exit;
}
if($_REQUEST['request'] == 'getPDF'){
	$str = $_REQUEST['str'];
	$category = $_REQUEST['category'];
	//$products = Mage::getModel('catalog/product')->getCollection()->addFilter('sku','p1');
	$products = Mage::getModel('catalog/product')->getCollection();
	$products->addAttributeToFilter('visibility', array('eq' => '4'));
	$products->addAttributeToFilter('status', array('eq' => '1'));
	if($str !=""){
		$products->addAttributeToFilter(array(
				array(
					'attribute' => 'name',
					'like' => '%'.$str.'%'),
				array(
					'attribute' => 'sku',
					'like' => '%'.$str.'%')	
			));
	}
	if($category !=""){
		$products->joinField('category_id','catalog/category_product','category_id','product_id=entity_id',null,'left');
		$products->addAttributeToFilter('category_id', array('in' => array($category)));
	}		
	$producttechnical_info = array();
	foreach($products as $prod) {
		$product = Mage::getModel('catalog/product')->load($prod->getId());
			for($i = 1 ; $i <= 3 ; $i++){
				if($product->getData('technical_info'.$i) !=""){
					if($product->getData('technical_info'.$i) !="" && strpos($product->getData('technical_info'.$i),'.pdf')){
						$temp = array();
						$temp['id'] = $prod->getId();
						$temp['pdf'] = $product->getData('technical_info'.$i);
						array_push($producttechnical_info,$temp);
						//array_push($producttechnical_info,$product->getData('technical_info'.$i));
					}
				}
			}
	}
   if($producttechnical_info && count($producttechnical_info) > 0){	
	  $title = Mage::getModel('catalog/product')->load($producttechnical_info[0]['id'])->getName();
	  $short_description = Mage::getModel('catalog/product')->load($producttechnical_info[0]['id'])->getShortDescription();
	  $description = Mage::getModel('catalog/product')->load($producttechnical_info[0]['id'])->getDescription();	
  }	
	$a=0;
  $showThumbnail = false;

  if($producttechnical_info && count($producttechnical_info) >= 1){
	$showThumbnail = true;
  }
 ?>
 <?php if($producttechnical_info && count($producttechnical_info) > 0){?>
<table width="100%" border="0" >
          <tr>
            <td valign="top" align="center" id="productPDFFrame">
              <?php echo "<iframe  name=\"productPDFFrame\"  src=".$producttechnical_info[0]['pdf']." width=\"100%\" height=\"960\" frameborder=\"0\"></iframe>";?>
              </td>
              <td id="product_detail">
            	<div class="product-name" >
                  <h1><?php echo $title;?></h1>
                </div>
                <div class="short-description">
                  <div class="std"><?php echo $short_description;?></div>
                </div>
            </td>
          </tr>
          <tr>
            <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><ul class='video_thumb'>
                <?php foreach($producttechnical_info as $pdf){
					  $row = $pdf['pdf'];
					  $fileName =  urldecode(basename($row));
					  $fileName = str_replace(".pdf","",$fileName);
					  $fileName = str_replace(" ","-",strtolower($fileName));
					  if(file_exists(Mage::getBaseDir('media').'/technical_info/image/'.$fileName.'.jpg')){
						  $thumb = Mage::getBaseUrl('media').'technical_info/image/'.$fileName.'.jpg';
					  }else{
						  exec('convert -thumbnail 256x150 '.$row.'[0] media/technical_info/image/'.$fileName.'.jpg');
						  $thumb = Mage::getBaseUrl('media').'technical_info/image/'.$fileName.'.jpg';
					  }
					?>
					<li><a href="javascript:getPdf('<?php echo $row;?>','<?php echo $pdf['id'];?>');" title="<?php echo $fileName;?>"><img src="<?php echo  $thumb;?>"  /></a></li>
					<?php }?>
              </ul></td>
          </tr>
        </table>
<?php }else{?>
	<table width="100%" border="0" >
  <tr>
    <td valign="top"><h3>No Product's Technical Info Found. Please try again!</h3></td>
  </tr>
</table>
<?php }?>
<?php
exit;
}
?>